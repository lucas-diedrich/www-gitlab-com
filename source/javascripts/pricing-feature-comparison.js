var pricingFeatureComparison = function() {
  var expandableTargets = document.querySelectorAll('.feature-list-view-more');
  for(loopcount=0;loopcount<expandableTargets.length;loopcount++)
  {
    expandableTargets[loopcount].addEventListener('click', function(event)
    {
      var iterationValue = event.currentTarget.getAttribute('data-flvm');
      var featureListIteration = '.feature-list[data-flvm="' + iterationValue + '"]';
      var featureListViewMoreIteration = '.feature-list-view-more[data-flvm="' + iterationValue + '"]';
      document.querySelector(featureListIteration).classList.toggle("expanded");
      document.querySelector(featureListViewMoreIteration).classList.toggle("expanded");
      if (document.querySelector(featureListViewMoreIteration).classList.contains("expanded") == false)
      {
        document.querySelector(featureListViewMoreIteration).scrollIntoView({ behavior: 'smooth', block: 'center' });
      };
    });
  };
};

window.pricingFeatureComparison = pricingFeatureComparison;
