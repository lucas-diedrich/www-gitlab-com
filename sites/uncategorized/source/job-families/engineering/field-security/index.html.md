---
layout: job_family_page
title: "Field Security"
extra_js:
- libs/mermaid.min.js
---

As members of GitLab's [Security Assurance sub department](/handbook/engineering/security/security-assurance/security-assurance.html), the [Field Security team](/handbook/engineering/security/security-assurance/risk-field-security/) serves as the public representation of GitLab's internal Security function. The team is tasked with providing high levels of security assurance to internal and external customers through customer support, sales enablement and security evangelism programs.

## Responsibilities
* Professionally handle communications with internal and external stakeholders
* Maintain up-to-date knowledge of GitLab's product, environment, systems and architecture
* Educate internal and external stakeholders on GitLab’s Security practices through formal and informal training, handbook improvements, white papers, conference presentations and blog posts
* Gather and report on established metrics within the field security program

## Requirements
* Capability to use GitLab
* Exemplary written and verbal communication and presentation skills
* Prior experience working with a SaaS company preferred

## Levels

### Field Security Engineer (Intermediate)
This position reports to the Manager, Field Security at GitLab.  

#### Field Security Engineer (Intermediate) Job Grade
The Field Security Engineer is a [6](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Field Security Engineer (Intermediate) Responsibilities
* Complete customer security assessments, questionnaires and sales enablement activities
* Maintain the Customer Assurance Package and other self-service customer security resources
* Maintain GitLab's standard security response database (RFP)
* Triage new or changing security requirements, security issues, and/or customer risks
* Maintain handbook pages, policies, standards, procedures and runbooks related to Field Security
* Identify opportunities for Field Security process automation
* Maintain Field Security automation tasks

#### Field Security Engineer (Intermediate) Requirements
* At least 2 years of experience conducting customer assurance activities
* Demonstrated experience with at least two security control frameworks such as: SOC 2, ISO, NIST, COSO, COBIT
* Working understanding of how security works with cloud-native technology stacks

### Senior Field Security Engineer
This position reports to the Manager, Field Security at GitLab.  

#### Senior Field Security Engineer Job Grade
The Senior Field Security Engineer is a [7](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Senior Field Security Engineer Responsibilities
* Extends the Field Security Engineer responsibilities
* Execute sales enablement activities, including customer security assessments and contract reviews
* Execute end to end Field Security initiatives in accordance with the compliance roadmap
* Mature the Customer Assurance Package and other self-service customer security resources
* Monitor industry trends and demands to position GitLab as an industry leader in Security and execute initiatives to support these trends
* Execute peer reviews and provide meaningful feedback 
* Design requirements for Field Security automation tasks
* Recommend new Field Security metrics and automate reporting of existing metrics

#### Senior Field Security Engineer Requirements
* Ability to use GitLab
* At least 5 years of experience conducting customer assurance activities
* Demonstrated experience with at least four security control frameworks such as: SOC 2, ISO, NIST, COSO, COBIT
* Demonstrated industry security experience, particularly in DevSecOps, Application Security and/or Cloud-Native Security

### Staff Field Security Engineer
This position reports to the Manager, Field Security at GitLab.  

#### Staff Field Security Engineer Job Grade
The Staff Field Security Engineer is a [8](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Staff Field Security Engineer Responsibilities
* Extends the Senior Field Security Engineer responsibilities
* Proficient knowledge of GitLab's product, environment, systems and architecture while mentoring others on this knowledge and helping to shape design decisions for the sake of security compliance efficiencies
* Mentor other Field Security Engineers and improve quality and quantity of the team's output
* Design and implement major iterations of Field Security programs in alignment with industry trends, predictions and customer demands
* Participate in Field Security roadmap development based on customer needs
* Present a minimum of 4 external facing engagements per annum, ex: Commit, conferences, guest speaking engagements, blog posts, whitepapers
* Create dynamic open-source Field Security programs that deliver value to the GitLab community
* Design, develop, and deploy scripts to automate administrative and process tasks related to Field Security
* Design, develop, and deploy an automated metric reporting for all Field Security programs

#### Staff Field Security Engineer Requirements
* At least 10 years of experience conducting customer assurance activities
* Proficient experience with at least six security control frameworks such as: SOC 2, ISO, NIST, COSO, COBIT
* Demonstrated industry security experience, particularly in DevSecOps, Application Security and/or Cloud-Native Security

### Manager, Field Security 
The Field Security Manager reports directly to the Security Assurance Director.

#### Manager, Field Security Job Grade
The Field Security Manager is a [8](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Manager, Field Security Responsibilities
* Evangelize security across GitLab and with our customers
* Hire and oversee a world class team of security engineers and Engineers
* Build a strong, collaborative partnership with Security, Infrastructure, Sales and Product teams
* Manage customer calls, contract reviews and/or assessments providing assurance on GitLab security
* Assess and promote customer security requests across the GitLab security and product teams
* Prepare and deliver meaningful metrics to Security Assurance leadership
* Identify and implement automation of manual processes to shorten review and request cycles
* Successfully execute on quarterly OKRs
 
#### Manager, Field Security Requirements
* Exceptional communication skills, including verbal, written, and presentation skills to a variety of stakeholders
* At least 3 years prior experience managing information security customer facing teams
* Working knowledge of common information security management frameworks, regulatory requirements and applicable standards such as: ISO 27001, SOC 2, HIPAA, GDPR, PCI, SOX, etc.

### Senior Manager, Field Security 
The Field Security Manager reports directly to the Security Assurance Director.

#### Senior Manager, Field Security Job Grade
The Field Security Senior Manager is a [9](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Senior Manager, Field Security Responsibilities
* Extends the Manager, Field Security responsibilities
* Create and deploy innovative and effective strategies for proactively addressing prospective and customer security requests
* Maintain reliable, up-to-date, information across the industry regarding new security trends, threats and vulnerabilities
* Present a minimum of 6 external facing engagements per annum, ex: Commit, conferences, guest speaking engagements, blog posts, whitepapers
* Draft and successfully execute on quarterly OKRs
 
#### Senior Manager, Field Security Requirements
* At least 6 years prior experience managing information security customer facing teams
* Detailed knowledge of common information security management frameworks, regulatory requirements and applicable standards such as: ISO 27001, SOC 2, HIPAA, GDPR, PCI, SOX, etc.

## Segment
### Security Leadership
For details on the Security organization leadership roles, to include the Security Assurance Director and VP of Security, see the Security Leadership page.

## Performance Indicators
* [Security Impact on ARR](https://about.gitlab.com/handbook/engineering/security/performance-indicators/#security-impact-on-iacv)

## Career Ladder
```mermaid
  graph LR;
  sec:se(Field Security Engineer)-->sec:sse(Senior Field Security Engineer);
  sec:sse(Senior Field Security Engineer)-->sec:stse(Staff Field Security Engineer);
  sec:sse(Senior Field Security Engineer)-->sec:sem(Manager, Field Security);
  sec:sem(Manager, Field Security)-->sec:sesm(Senior Manager, Field Security);
  sec:sesm(Senior Manager, Field Security)-->sec:ds(Director, Security Assurance);
```

## Hiring Process
Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find their job title on our [team page](/company/team/).
* Qualified candidates will be invited to schedule a 30 minute [screening call](/handbook/hiring/interviewing/#screening-call) with one of our Global Recruiters.
* Next, candidates will be invited to schedule a 50-minute interviews with the hiring manager, 
* Then, candidates will be invited to schedule 3 separate 50-minute interviews with 3 different peers from within the Security orgnaization,
* Finally, candidates will be invited to schedule a 25-minute interview with the Director, Security Risk and Compliance.

Additional details about our process can be found on our [hiring page](/handbook/hiring).
